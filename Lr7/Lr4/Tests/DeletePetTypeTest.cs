using NUnit.Framework;
using Lr7.PageObjects;
using Lr7.PageFactory;
using Lr7.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class DeletePetTypeTest : BaseTest
    {
        [Test, Description("This test checks that pet type can be deleted successfully")]
        [AllureSuite("Pet types")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void DeletePetType()
        {
            OptionComponent option = Components.Button;
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            option.OpenPetTypes();
            Helpers.Wait();
            petTypes.DeletePetType();
            Helpers.Wait();
            Assert.True(petTypes.isCountDecreased());
        }
    }
}