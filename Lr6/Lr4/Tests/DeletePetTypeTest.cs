using NUnit.Framework;
using Lr6.PageObjects;
using Lr6.PageFactory;
using Lr6.PageComponents;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr6
{
    [TestFixture]
    [AllureNUnit]
    public class DeletePetTypeTest : BaseTest
    {
        [Test, Description("This test checks that pet type can be deleted successfully")]
        [AllureSuite("Pet types")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void DeletePetType()
        {
            OptionComponent option = Components.Button;
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            option.OpenPetTypes();
            Helpers.Wait();
            petTypes.DeletePetType();
            Helpers.Wait();
            Assert.True(petTypes.isCountDecreased());
        }
    }
}