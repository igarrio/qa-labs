using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class DeleteSpecialtyTest : BaseTest
    {
        [Test]
        public void DeleteSpecialty()
        {
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            specialties.OpenSpecialties();
            Wait();
            specialties.DeleteSpecialty();
            Wait();
            Assert.True(specialties.isCountDescreased());
        }
    }
}