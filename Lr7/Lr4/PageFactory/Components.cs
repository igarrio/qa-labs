﻿using Lr7.PageComponents;

namespace Lr7.PageFactory
{
    public static class Components
    {
        public static OptionComponent Button => new OptionComponent(BaseTest.driver);
    }
}
