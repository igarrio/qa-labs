using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class ReturnToHomeFromSpecialtiesTest : BaseTest
    {
        [Test]
        public void ReturnToHomeFromSpecialties()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            specialties.ReturnToHomePage();
            HomePageObject home = new HomePageObject(driver);
            Assert.True(home.GetHomePageName() == "Welcome to Petclinic");
        }
    }
}