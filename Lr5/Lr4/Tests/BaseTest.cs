﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Lr5
{
    public abstract class BaseTest
    {
        public static IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://client.sana-commerce.dev");
            driver.Manage().Window.Size = new System.Drawing.Size(1074, 676);
        }

        [TearDown]
        protected void TearDown()
        {
            driver.Quit();
        }
    }
}
