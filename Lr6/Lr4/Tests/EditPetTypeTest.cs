using NUnit.Framework;
using Lr6.PageObjects;
using Lr6.PageComponents;
using Lr6.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr6
{
    [TestFixture]
    [AllureNUnit]
    public class EditPetTypeTest : BaseTest
    {
        [Test, Description("This test checks that pet type can be edited successfully")]
        [AllureSuite("Pet types")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void EditPetType()
        {
            OptionComponent option = Components.Button;
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            option.OpenPetTypes();
            Helpers.Wait();
            petTypes.OpenPetTypeEditPage();
            petTypes.EditPetType("test1");
            Helpers.Wait();
            Assert.True(petTypes.IsNameCorrect("test1"));
        }
    }
}