using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class EditPetTypeTest : BaseTest
    {
        [Test]
        public void EditPetType()
        {
            OptionComponent option = Components.Button;
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            option.OpenPetTypes();
            Helpers.Wait();
            petTypes.OpenPetTypeEditPage();
            petTypes.EditPetType("test1");
            Helpers.Wait();
            Assert.True(petTypes.IsNameCorrect("test1"));
        }
    }
}