using NUnit.Framework;
using Lr7.PageObjects;
using Lr7.PageComponents;
using Lr7.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class EditPetTypeTest : BaseTest
    {
        [TestCase("test1")]
        [TestCase("test2")]
        [TestCase("test3")]
        [TestCase("test4")]
        [TestCase("test5")]
        [Test, Description("This test checks that pet type can be edited successfully")]
        [AllureSuite("Pet types")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void EditPetType(string name)
        {
            OptionComponent option = Components.Button;
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            option.OpenPetTypes();
            Helpers.Wait();
            petTypes.OpenPetTypeEditPage();
            petTypes.EditPetType(name);
            Helpers.Wait();
            Assert.True(petTypes.IsNameCorrect(name));
        }
    }
}