﻿using Allure.Commons;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace Lr7
{
    public abstract class BaseTest
    {
        public static IWebDriver driver;

        Dictionary<string, object> additionalSelenoidCapabilities = new Dictionary<string, object>();

        [SetUp]
        public void Setup()
        {
            additionalSelenoidCapabilities["name"] = "Simple test";
            additionalSelenoidCapabilities["enableVNC"] = true;
            additionalSelenoidCapabilities["enableVideo"] = true;
            var chrome_options = new ChromeOptions();
            chrome_options.AddAdditionalOption("selenoid:options", additionalSelenoidCapabilities);
            driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), chrome_options.ToCapabilities());
            driver.Navigate().GoToUrl("https://client.sana-commerce.dev");
            driver.Manage().Window.Size = new System.Drawing.Size(1074, 676);
        }

        [TearDown]
        protected void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                var filename = TestContext.CurrentContext.Test.MethodName + "_screenshot_" + DateTime.Now.Ticks + ".png";
                var path = @"C:\Users\migar\Desktop\QA_Labs\Lr6\Lr4\TestResults";
                screenshot.SaveAsFile(path, ScreenshotImageFormat.Png);
                AllureLifecycle.Instance.AddAttachment(filename, "image/png", path);
            }
            driver.Quit();
        }
    }
}
