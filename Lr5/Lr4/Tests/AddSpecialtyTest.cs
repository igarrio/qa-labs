using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class AddSpecialtyTest : BaseTest
    {
        [Test]
        public void AddSpecialty()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            specialties.OpenSpecialtyAddForm();
            specialties.AddSpecialty("test");
            Helpers.Wait();
            Assert.True(specialties.IsNameCorrect("test"));
        }
    }
}