﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lr6.PageObjects
{
    public class PetTypesPageObject : BasePageObject
    {
        public PetTypesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By AddPetTypeButton = By.CssSelector(".addPet");
        private By PetTypeName = By.Id("name");
        private By PetTypeSaveButton = By.CssSelector(".saveType");
        private By PetTypeItems = By.CssSelector("tr input");
        private By PetTypeItemName = By.CssSelector("tr:last-child input");
        private By DeletePetTypeButton = By.CssSelector("tr:last-child .deletePet");
        private By EditPetTypeButton = By.CssSelector("tr:last-child .editPet");
        private By UpdatePetTypeButton = By.CssSelector(".updatePetType");
        private By PetTypePageName = By.CssSelector("h2");

        private int count;

        [AllureStep("Get pet types count")]
        private int GetPetTypesCount()
        {
            return driver.FindElements(PetTypeItems).Count;
        }

        [AllureStep("Open pet type add form")]
        public void OpenPetTypeAddForm()
        {
            driver.FindElement(AddPetTypeButton).Click();
        }

        [AllureStep("Add pet type")]
        public void AddPetType(string name)
        {
            Helpers.ClickSendKeys(driver.FindElement(PetTypeName), name);
            driver.FindElement(PetTypeSaveButton).Click();
        }

        [AllureStep("Delete pet type")]
        public void DeletePetType()
        {
            count = GetPetTypesCount();
            driver.FindElement(DeletePetTypeButton).Click();
        }

        [AllureStep("Open pet type edit page")]
        public void OpenPetTypeEditPage()
        {
            driver.FindElement(EditPetTypeButton).Click();
        }

        [AllureStep("Edit pet type")]
        public void EditPetType(string name)
        {
            Helpers.ClickClearSendKeys(driver.FindElement(PetTypeName), name);
            driver.FindElement(UpdatePetTypeButton).Click();
        }

        [AllureStep("Check is name correct")]
        public bool IsNameCorrect(string name)
        {
            string value = driver.FindElement(PetTypeItemName).GetAttribute("value");
            return value == name;
        }

        [AllureStep("Check is count decreased")]
        public bool isCountDecreased()
        {
            return GetPetTypesCount() < count;
        }

        [AllureStep("Get pet type page name")]
        public string GetPetTypePageName()
        {
            return driver.FindElement(PetTypePageName).Text;
        }
    }
}
