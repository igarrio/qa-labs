﻿using OpenQA.Selenium;

namespace Lr5.PageObjects
{
    public class SpecialtiesPageObject : BasePageObject
    {
        public SpecialtiesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By AddSpecialtyButton = By.CssSelector(".addSpecialty");
        private By SpecialtyName = By.Id("name");
        private By SpecialtySaveButton = By.CssSelector(".addSpecialty");
        private By SpecialtyItems = By.CssSelector("tr input");
        private By SpecialtyItemName = By.CssSelector("tr:last-child input");
        private By DeleteSpecialtyButton = By.CssSelector("tr:last-child .deleteSpecialty");
        private By EditSpecialtyButton = By.CssSelector("tr:last-child .editSpecialty");
        private By UpdateSpecialtyButton = By.CssSelector(".updateSpecialty");
        private By SpecialtyPageName = By.CssSelector("h2");
        private By CancelSpecialtyEditButton = By.CssSelector(".cancelUpdate");
        private By HomeSpecialtiesButton = By.CssSelector(".returnHome");

        private int count;

        private int GetSpecialtiesCount()
        {
            return driver.FindElements(SpecialtyItems).Count;
        }

        public void OpenSpecialtyAddForm()
        {
            driver.FindElement(AddSpecialtyButton).Click();
        }

        public void AddSpecialty(string name)
        {
            Helpers.ClickSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(SpecialtySaveButton).Click();
        }

        public void DeleteSpecialty()
        {
            count = GetSpecialtiesCount();
            driver.FindElement(DeleteSpecialtyButton).Click();
        }

        public void OpenSpecialtyEditPage()
        {
            driver.FindElement(EditSpecialtyButton).Click();
        }

        public void EditSpecialty(string name)
        {
            Helpers.ClickClearSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(UpdateSpecialtyButton).Click();
        }

        public void ReturnToHomePage()
        {
            driver.FindElement(HomeSpecialtiesButton).Click();
        }

        public void CancelEditing(string name)
        {
            Helpers.ClickSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(CancelSpecialtyEditButton).Click();
        }

        public bool IsNameCorrect(string name)
        {
            string value = driver.FindElement(SpecialtyItemName).GetAttribute("value");
            return value == name;
        }

        public bool isCountDescreased()
        {
            return GetSpecialtiesCount() < count;
        }

        public string GetSpecialtyPageName()
        {
            return driver.FindElement(SpecialtyPageName).Text;
        }

        public bool IsPageNameCorrect(string name)
        {
            return driver.FindElement(SpecialtyPageName).Text == name;
        }
    }
}
