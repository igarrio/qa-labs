using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class EditPetTypeTest : BaseTest
    {
        [Test]
        public void EditPetType()
        {
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            petTypes.OpenPetTypes();
            Wait();
            petTypes.OpenPetTypeEditPage();
            petTypes.EditPetType("test1");
            Wait();
            Assert.True(petTypes.IsNameCorrect("test1"));
        }
    }
}