using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class CancelSpecialtyEditingTest : BaseTest
    {
        [Test]
        public void CancelSpecialtyEditing()
        {
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            specialties.OpenSpecialties();
            Wait();
            specialties.OpenSpecialtyEditPage();
            specialties.CancelEditing("test12");
            Wait();
            Assert.False(specialties.IsNameCorrect("test12"));
        }
    }
}