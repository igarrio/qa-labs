using NUnit.Framework;
using Lr7.PageObjects;
using Lr7.PageComponents;
using Lr7.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class EditSpecialtyTest : BaseTest
    {
        static object[] TestData = { "test1", "test2", "test3", "test4", "test5" };
        [Test, Description("This test checks that specialty can be edited successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        [TestCaseSource(nameof(TestData))]
        public void EditSpecialty(string name)
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Helpers.Wait();
            specialties.OpenSpecialtyEditPage();
            specialties.EditSpecialty(name);
            Helpers.Wait();
            Assert.True(specialties.IsNameCorrect(name));
        }
    }
}