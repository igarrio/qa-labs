using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class AddSpecialtyTest : BaseTest
    {
        [Test]
        public void AddSpecialty()
        {
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            specialties.OpenSpecialties();
            specialties.OpenSpecialtyAddForm();
            specialties.AddSpecialty("test");
            Wait();
            Assert.True(specialties.IsNameCorrect("test"));
        }
    }
}