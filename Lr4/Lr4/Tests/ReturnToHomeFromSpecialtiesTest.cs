using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class ReturnToHomeFromSpecialtiesTest : BaseTest
    {
        [Test]
        public void ReturnToHomeFromSpecialties()
        {
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            specialties.OpenSpecialties();
            specialties.ReturnToHomePage();
            HomePageObject home = new HomePageObject(driver);
            Assert.True(home.GetHomePageName() == "Welcome to Petclinic");
        }
    }
}