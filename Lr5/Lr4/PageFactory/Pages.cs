﻿using Lr5.PageObjects;

namespace Lr5.PageFactory
{
    public static class Pages
    {
        public static HomePageObject Home => new HomePageObject(BaseTest.driver);
        public static PetTypesPageObject PetTypes => new PetTypesPageObject(BaseTest.driver);
        public static SpecialtiesPageObject Vets => new SpecialtiesPageObject(BaseTest.driver);
    }
}
