using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class LoadPetTypesTest : BaseTest
    {
        [Test]
        public void LoadPetTypes()
        {
            OptionComponent option = Components.Button;
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            option.OpenPetTypes();
            Assert.True(petTypes.GetPetTypePageName() == "Pet Types");
        }
    }
}