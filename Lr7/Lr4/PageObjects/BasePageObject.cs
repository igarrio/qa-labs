﻿using OpenQA.Selenium;

namespace Lr7.PageObjects
{
    public abstract class BasePageObject
    {
        protected IWebDriver driver;
        protected Dictionary<string, object> vars;

        public BasePageObject(IWebDriver driver)
        {
            this.driver = driver;
            vars = new Dictionary<string, object>();
        }
    }
}
