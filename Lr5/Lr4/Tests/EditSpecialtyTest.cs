using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class EditSpecialtyTest : BaseTest
    {
        [Test]
        public void EditSpecialty()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Helpers.Wait();
            specialties.OpenSpecialtyEditPage();
            specialties.EditSpecialty("test1");
            Helpers.Wait();
            Assert.True(specialties.IsNameCorrect("test1"));
        }
    }
}