using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class AddPetTypeTest : BaseTest
    {
        [Test]
        public void AddPetType()
        {
            OptionComponent option = Components.Button;
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            option.OpenPetTypes();
            petTypes.OpenPetTypeAddForm();
            petTypes.AddPetType("test");
            Helpers.Wait();
            Assert.True(petTypes.IsNameCorrect("test"));
        }
    }
}