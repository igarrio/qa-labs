using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class LoadPetTypesTest : BaseTest
    {
        [Test]
        public void LoadPetTypes()
        {
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            petTypes.OpenPetTypes();
            Assert.True(petTypes.GetPetTypePageName() == "Pet Types");
        }
    }
}