using NUnit.Framework;
using Lr7.PageObjects;
using Lr7.PageComponents;
using Lr7.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class CancelSpecialtyEditingTest : BaseTest
    {
        [Test, Description("This test checks that specialty editing can be canceled successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void CancelSpecialtyEditing([Values("test12", "test23", "test34", "test45", "test56")] string name)
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Helpers.Wait();
            specialties.OpenSpecialtyEditPage();
            specialties.CancelEditing(name);
            Helpers.Wait();
            Assert.False(specialties.IsNameCorrect(name));
        }
    }
}