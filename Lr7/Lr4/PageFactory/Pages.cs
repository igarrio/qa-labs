﻿using Lr7.PageObjects;

namespace Lr7.PageFactory
{
    public static class Pages
    {
        public static HomePageObject Home => new HomePageObject(BaseTest.driver);
        public static PetTypesPageObject PetTypes => new PetTypesPageObject(BaseTest.driver);
        public static SpecialtiesPageObject Vets => new SpecialtiesPageObject(BaseTest.driver);
    }
}
