﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    internal class HomePageObject : BasePageObject
    {
        public HomePageObject(IWebDriver driver) : base(driver)
        {

        }

        private By HomePageName = By.CssSelector("h1");

        public string GetHomePageName()
        {
            return driver.FindElement(HomePageName).Text;
        }
    }
}
