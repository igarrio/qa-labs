﻿using OpenQA.Selenium;

namespace Lr6
{
    class Helpers
    {
        public static void ClickSendKeys(IWebElement webElement, string text)
        {
            webElement.Click();
            webElement.SendKeys(text);
        }

        public static void ClickClearSendKeys(IWebElement webElement, string text)
        {
            webElement.Click();
            webElement.Clear();
            webElement.SendKeys(text);
        }

        public static void Wait()
        {
            Thread.Sleep(2000);
        }
    }
}
