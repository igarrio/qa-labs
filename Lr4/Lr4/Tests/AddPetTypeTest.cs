using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class AddPetTypeTest : BaseTest
    {
        [Test]
        public void AddPetType()
        {
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            petTypes.OpenPetTypes();
            petTypes.OpenPetTypeAddForm();
            petTypes.AddPetType("test");
            Wait();
            Assert.True(petTypes.IsNameCorrect("test"));
        }
    }
}