using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class DeletePetTypeTest : BaseTest
    {
        [Test]
        public void DeletePetType()
        {
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            petTypes.OpenPetTypes();
            Wait();
            petTypes.DeletePetType();
            Wait();
            Assert.True(petTypes.isCountDescreased());
        }
    }
}