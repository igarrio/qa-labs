using NUnit.Framework;
using Lr7.PageObjects;
using Lr7.PageComponents;
using Lr7.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class DeleteSpecialtyTest : BaseTest
    {
        [Test, Description("This test checks that specialty can be deleted successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void DeleteSpecialty()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Helpers.Wait();
            specialties.DeleteSpecialty();
            Helpers.Wait();
            Assert.True(specialties.isCountDescreased());
        }
    }
}