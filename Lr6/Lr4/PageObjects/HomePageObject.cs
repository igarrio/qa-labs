﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lr6.PageObjects
{
    public class HomePageObject : BasePageObject
    {
        public HomePageObject(IWebDriver driver) : base(driver)
        {

        }

        private By HomePageName = By.CssSelector("h1");

        [AllureStep("Get home page name")]
        public string GetHomePageName()
        {
            return driver.FindElement(HomePageName).Text;
        }
    }
}
