﻿using OpenQA.Selenium;

namespace Lr5.PageComponents
{
    public class OptionComponent
    {
        private IWebDriver driver;

        public OptionComponent(IWebDriver driver) => this.driver = driver;

        public IWebElement PetTypeOption() => driver.FindElement(By.CssSelector("li:nth-child(4) span:nth-child(2)"));
        public IWebElement SpecialtyOption() => driver.FindElement(By.CssSelector("li:nth-child(5) span:nth-child(2)"));

        public void OpenPetTypes()
        {
            PetTypeOption().Click();
        }

        public void OpenSpecialties()
        {
            SpecialtyOption().Click();
        }
    }
}
