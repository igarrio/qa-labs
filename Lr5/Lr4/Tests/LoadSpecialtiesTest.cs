using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class LoadSpecialtiesTest : BaseTest
    {
        [Test]
        public void LoadSpecialties()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Assert.True(specialties.GetSpecialtyPageName() == "Specialties");
        }
    }
}