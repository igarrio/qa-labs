using NUnit.Framework;
using Lr7.PageObjects;
using Lr7.PageComponents;
using Lr7.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class ReturnToHomeFromSpecialtiesTest : BaseTest
    {
        [Test, Description("This test checks that we can return from specialties page to home page")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void ReturnToHomeFromSpecialties()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            specialties.ReturnToHomePage();
            HomePageObject home = new HomePageObject(driver);
            Assert.True(home.GetHomePageName() == "Welcome to Petclinic");
        }
    }
}