using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageFactory;
using Lr5.PageComponents;

namespace Lr5
{
    [TestFixture]
    public class DeletePetTypeTest : BaseTest
    {
        [Test]
        public void DeletePetType()
        {
            OptionComponent option = Components.Button;
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            option.OpenPetTypes();
            Helpers.Wait();
            petTypes.DeletePetType();
            Helpers.Wait();
            Assert.True(petTypes.isCountDescreased());
        }
    }
}