using NUnit.Framework;
using Lr6.PageObjects;
using Lr6.PageComponents;
using Lr6.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr6
{
    [TestFixture]
    [AllureNUnit]
    public class EditSpecialtyTest : BaseTest
    {
        [Test, Description("This test checks that specialty can be edited successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void EditSpecialty()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Helpers.Wait();
            specialties.OpenSpecialtyEditPage();
            specialties.EditSpecialty("test1");
            Helpers.Wait();
            Assert.True(specialties.IsNameCorrect("test1"));
        }
    }
}