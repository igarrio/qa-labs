using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class DeleteSpecialtyTest : BaseTest
    {
        [Test]
        public void DeleteSpecialty()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Helpers.Wait();
            specialties.DeleteSpecialty();
            Helpers.Wait();
            Assert.True(specialties.isCountDescreased());
        }
    }
}