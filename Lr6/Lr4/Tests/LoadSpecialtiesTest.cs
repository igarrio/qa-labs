using NUnit.Framework;
using Lr6.PageObjects;
using Lr6.PageComponents;
using Lr6.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr6
{
    [TestFixture]
    [AllureNUnit]
    public class LoadSpecialtiesTest : BaseTest
    {
        [Test, Description("This test checks that specialties page can be loaded successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void LoadSpecialties()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Assert.True(specialties.GetSpecialtyPageName() == "Specialties");
        }
    }
}