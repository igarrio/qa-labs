﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lr6.PageObjects
{
    public class SpecialtiesPageObject : BasePageObject
    {
        public SpecialtiesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By AddSpecialtyButton = By.CssSelector(".addSpecialty");
        private By SpecialtyName = By.Id("name");
        private By SpecialtySaveButton = By.CssSelector(".addSpecialty");
        private By SpecialtyItems = By.CssSelector("tr input");
        private By SpecialtyItemName = By.CssSelector("tr:last-child input");
        private By DeleteSpecialtyButton = By.CssSelector("tr:last-child .deleteSpecialty");
        private By EditSpecialtyButton = By.CssSelector("tr:last-child .editSpecialty");
        private By UpdateSpecialtyButton = By.CssSelector(".updateSpecialty");
        private By SpecialtyPageName = By.CssSelector("h2");
        private By CancelSpecialtyEditButton = By.CssSelector(".cancelUpdate");
        private By HomeSpecialtiesButton = By.CssSelector(".returnHome");

        private int count;

        [AllureStep("Get specialties count")]
        private int GetSpecialtiesCount()
        {
            return driver.FindElements(SpecialtyItems).Count;
        }

        [AllureStep("Open specialty add form")]
        public void OpenSpecialtyAddForm()
        {
            driver.FindElement(AddSpecialtyButton).Click();
        }

        [AllureStep("Add specialty")]
        public void AddSpecialty(string name)
        {
            Helpers.ClickSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(SpecialtySaveButton).Click();
        }

        [AllureStep("Delete specialty")]
        public void DeleteSpecialty()
        {
            count = GetSpecialtiesCount();
            driver.FindElement(DeleteSpecialtyButton).Click();
        }

        [AllureStep("Open specialty edit page")]
        public void OpenSpecialtyEditPage()
        {
            driver.FindElement(EditSpecialtyButton).Click();
        }

        [AllureStep("Edit specialty")]
        public void EditSpecialty(string name)
        {
            Helpers.ClickClearSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(UpdateSpecialtyButton).Click();
        }

        [AllureStep("Return to home page")]
        public void ReturnToHomePage()
        {
            driver.FindElement(HomeSpecialtiesButton).Click();
        }

        [AllureStep("Cancel specialty editing")]
        public void CancelEditing(string name)
        {
            Helpers.ClickSendKeys(driver.FindElement(SpecialtyName), name);
            driver.FindElement(CancelSpecialtyEditButton).Click();
        }

        [AllureStep("Check is name correct")]
        public bool IsNameCorrect(string name)
        {
            string value = driver.FindElement(SpecialtyItemName).GetAttribute("value");
            return value == name;
        }

        [AllureStep("Check is count decreased")]
        public bool isCountDescreased()
        {
            return GetSpecialtiesCount() < count;
        }

        [AllureStep("Get specialty page name")]
        public string GetSpecialtyPageName()
        {
            return driver.FindElement(SpecialtyPageName).Text;
        }

        [AllureStep("Check is page name correct")]
        public bool IsPageNameCorrect(string name)
        {
            return driver.FindElement(SpecialtyPageName).Text == name;
        }
    }
}
