using NUnit.Framework;
using Lr5.PageObjects;
using Lr5.PageComponents;
using Lr5.PageFactory;

namespace Lr5
{
    [TestFixture]
    public class CancelSpecialtyEditingTest : BaseTest
    {
        [Test]
        public void CancelSpecialtyEditing()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Helpers.Wait();
            specialties.OpenSpecialtyEditPage();
            specialties.CancelEditing("test12");
            Helpers.Wait();
            Assert.False(specialties.IsNameCorrect("test12"));
        }
    }
}