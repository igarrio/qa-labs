﻿using OpenQA.Selenium;

namespace Lr5.PageObjects
{
    public class PetTypesPageObject : BasePageObject
    {
        public PetTypesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By AddPetTypeButton = By.CssSelector(".addPet");
        private By PetTypeName = By.Id("name");
        private By PetTypeSaveButton = By.CssSelector(".saveType");
        private By PetTypeItems = By.CssSelector("tr input");
        private By PetTypeItemName = By.CssSelector("tr:last-child input");
        private By DeletePetTypeButton = By.CssSelector("tr:last-child .deletePet");
        private By EditPetTypeButton = By.CssSelector("tr:last-child .editPet");
        private By UpdatePetTypeButton = By.CssSelector(".updatePetType");
        private By PetTypePageName = By.CssSelector("h2");

        private int count;

        private int GetPetTypesCount()
        {
            return driver.FindElements(PetTypeItems).Count;
        }

        public void OpenPetTypeAddForm()
        {
            driver.FindElement(AddPetTypeButton).Click();
        }

        public void AddPetType(string name)
        {
            Helpers.ClickSendKeys(driver.FindElement(PetTypeName), name);
            driver.FindElement(PetTypeSaveButton).Click();
        }

        public void DeletePetType()
        {
            count = GetPetTypesCount();
            driver.FindElement(DeletePetTypeButton).Click();
        }

        public void OpenPetTypeEditPage()
        {
            driver.FindElement(EditPetTypeButton).Click();
        }

        public void EditPetType(string name)
        {
            Helpers.ClickClearSendKeys(driver.FindElement(PetTypeName), name);
            driver.FindElement(UpdatePetTypeButton).Click();
        }

        public bool IsNameCorrect(string name)
        {
            string value = driver.FindElement(PetTypeItemName).GetAttribute("value");
            return value == name;
        }

        public bool isCountDescreased()
        {
            return GetPetTypesCount() < count;
        }

        public string GetPetTypePageName()
        {
            return driver.FindElement(PetTypePageName).Text;
        }
    }
}
