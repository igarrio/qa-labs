using NUnit.Framework;
using Lr6.PageObjects;
using Lr6.PageComponents;
using Lr6.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr6
{
    [TestFixture]
    [AllureNUnit]
    public class CancelSpecialtyEditingTest : BaseTest
    {
        [Test, Description("This test checks that specialty editing can be canceled successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void CancelSpecialtyEditing()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            Helpers.Wait();
            specialties.OpenSpecialtyEditPage();
            specialties.CancelEditing("test12");
            Helpers.Wait();
            Assert.False(specialties.IsNameCorrect("test12"));
        }
    }
}