﻿using Lr5.PageComponents;

namespace Lr5.PageFactory
{
    public static class Components
    {
        public static OptionComponent Button => new OptionComponent(BaseTest.driver);
    }
}
