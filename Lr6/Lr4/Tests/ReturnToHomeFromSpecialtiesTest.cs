using NUnit.Framework;
using Lr6.PageObjects;
using Lr6.PageComponents;
using Lr6.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr6
{
    [TestFixture]
    [AllureNUnit]
    public class ReturnToHomeFromSpecialtiesTest : BaseTest
    {
        [Test, Description("This test checks that we can return from specialties page to home page")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void ReturnToHomeFromSpecialties()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            specialties.ReturnToHomePage();
            HomePageObject home = new HomePageObject(driver);
            Assert.True(home.GetHomePageName() == "Welcome to Petclinic");
        }
    }
}