using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class EditSpecialtyTest : BaseTest
    {
        [Test]
        public void EditSpecialty()
        {
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            specialties.OpenSpecialties();
            Wait();
            specialties.OpenSpecialtyEditPage();
            specialties.EditSpecialty("test1");
            Wait();
            Assert.True(specialties.IsNameCorrect("test1"));
        }
    }
}