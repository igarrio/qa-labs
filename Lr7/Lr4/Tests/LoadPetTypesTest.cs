using NUnit.Framework;
using Lr7.PageObjects;
using Lr7.PageComponents;
using Lr7.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr7
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class LoadPetTypesTest : BaseTest
    {
        [Test, Description("This test checks that pet types poage can be loaded successfully")]
        [AllureSuite("Pet types")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void LoadPetTypes()
        {
            OptionComponent option = Components.Button;
            PetTypesPageObject petTypes = new PetTypesPageObject(driver);
            option.OpenPetTypes();
            Assert.True(petTypes.GetPetTypePageName() == "Pet Types");
        }
    }
}