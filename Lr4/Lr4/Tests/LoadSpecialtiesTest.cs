using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class LoadSpecialtiesTest : BaseTest
    {
        [Test]
        public void LoadSpecialties()
        {
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            specialties.OpenSpecialties();
            Assert.True(specialties.GetSpecialtyPageName() == "Specialties");
        }
    }
}