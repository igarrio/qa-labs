using NUnit.Framework;
using Lr6.PageObjects;
using Lr6.PageComponents;
using Lr6.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr6
{
    [TestFixture]
    [AllureNUnit]
    public class AddSpecialtyTest : BaseTest
    {
        [Test, Description("This test checks that specialty can be added successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void AddSpecialty()
        {
            OptionComponent option = Components.Button;
            SpecialtiesPageObject specialties = new SpecialtiesPageObject(driver);
            option.OpenSpecialties();
            specialties.OpenSpecialtyAddForm();
            specialties.AddSpecialty("test");
            Helpers.Wait();
            Assert.True(specialties.IsNameCorrect("test"));
        }
    }
}